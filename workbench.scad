/*
 * Draw a workbench.
 * 
 * This model can be used to plan the build of a workbench, using materials commonly available in DYI shops
 */

use <lib/framed_top.scad>
use <lib/leg.scad>

$vpd = 5000;

// Show exploded model
explode = false;
explode_mul = 2;    // Multiplier for exploded view


// Main table measurements
table_length = 1405;
table_width = 800;
table_height = 918;
table_top_tickness = 18;    // Tickness of the worktop

// Framing timber size
timber_top_width = 95;    // 4x2 inch timber in mm
timber_top_tickness = 45;

// Timber size for legs
timber_leg_width = timber_top_width;  // Same as for framing
timber_leg_tickness = timber_top_tickness;

// Overhang
oh_length = 45; // overhang from legs along the length
oh_width = 50;  // overhang from legs along the width
overhang_l_w = [timber_leg_tickness+oh_length, timber_leg_tickness+oh_width];

// Place the shelf higher (Default at the bottom)
shelf_h_add = 100;
// Timber size for shelf. Same as top, but can be set to smaller sizes
// for a lighter shelf 
timber_shelf_width = timber_top_width;
timber_shelf_tickness = timber_top_tickness;
table_shelf_tickness = table_top_tickness;


// Calculate frame size from worktop without overhang and leg tickness
frame_length = table_length - 2 * overhang_l_w[0];
frame_width = table_width - 2 * overhang_l_w[1];



// Legs
/*
*
* Draw the four legs
*
*/
leg_h = table_height - table_top_tickness;
explode_val = explode ? 2 * timber_leg_tickness * explode_mul : 0;

translate([frame_length/2 + explode_val, -frame_width/2 - explode_val, leg_h/2])
    leg(timber_leg_width, timber_leg_tickness, leg_h, "+x_-y");


translate([frame_length/2+explode_val, frame_width/2+explode_val, leg_h/2])
    leg(timber_leg_width, timber_leg_tickness, leg_h, "+x_+y");


translate([-frame_length/2-explode_val, frame_width/2+explode_val, leg_h/2])
    leg(timber_leg_width, timber_leg_tickness, leg_h, "-x_+y");

translate([-frame_length/2-explode_val, -frame_width/2-explode_val, leg_h/2])
    leg(timber_leg_width, timber_leg_tickness, leg_h, "-x_-y");



// Worktop
/*
*
* Draw a framed worktop with overhang
*
*/
echo("Worktop sheet size (l,w,t): ", table_length, ", ", table_width, ", ", table_top_tickness);
translate([0, 0, leg_h])
    framed_top("Worktop", timber_top_width, timber_top_tickness, frame_length, frame_width, table_top_tickness, overhang_l_w, explode, explode_mul);





// Shelf
/*
*
* Draw a framed shelf recessed in the legs
*
*/
echo("Shelf sheet size (l,w,t): ", frame_length, ", ", frame_width, ", ", table_shelf_tickness);
shelf_h = timber_shelf_width + table_shelf_tickness/2 + shelf_h_add;
translate([0, 0, shelf_h])
    framed_top("Shelf", timber_shelf_width, timber_shelf_tickness, frame_length, frame_width, table_shelf_tickness, [0,0], explode, explode_mul);

