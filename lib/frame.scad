use <stud.scad>

// Make a timber frame made of two long timber studs and three short studs 
module frame(timber_w, timber_t, length, width, explode=false, explode_mul=2){
    long_frame_length = length;
    short_frame_length = width - 2 * timber_t;
    center_offset = timber_t / 2; // Placement of blocks is relative to center
    side_distance_from_center = width / 2 - center_offset;
    top_distance_from_center = length / 2 - center_offset;
    
    explode_val = explode ? 1 * timber_t * explode_mul : 0;
    
    echo("Frame");
    
    // Draw front long side (-y)
    translate([0, -side_distance_from_center-explode_val, 0])
        stud("Front long stud", long_frame_length, timber_w, timber_t, hv="h_x");
    
    // Draw back long side (+y)
    translate([0, side_distance_from_center+explode_val, 0])
        stud("Back long stud", long_frame_length, timber_w, timber_t, hv="h_x");
    
    // Draw short side (-x)
    translate([-top_distance_from_center, 0, 0])
        stud("Left short stud", short_frame_length, timber_w, timber_t, hv="h_y");
    
    // Draw opposite short side (+x)
    translate([top_distance_from_center, 0, 0])
        stud("Right short stud", short_frame_length, timber_w, timber_t, hv="h_y");
    
    // Draw middle (x=0)
    stud("Middle short stud", short_frame_length, timber_w, timber_t, hv="h_y");
};


