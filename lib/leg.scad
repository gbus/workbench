use <stud.scad>


module sanity_check(timber_w, timber_t){
    if (timber_t > timber_w/2) {
        echo("WARNING: ", "To ensure stability of the workbench, the timber width should be about twice the tickness", "Tickness ", timber_t, "is more than half the width", timber_w/2);
    }
}

// Draw a workbench table leg
// A leg is made of two vertical timber studs in a L-shape, depending which quadrant of the x/y plane it occupies it has a different location value:
// "+x_-y": Front Left
// "+x_+y": Back Left
// "-x_+y": Back Right
// "-x_-y": Front Right
module leg(timber_w, timber_t, height, location){
    
    sanity_check(timber_w, timber_t);
    
    if (location == "+x_-y") {
        translate([-timber_w/2, -timber_t/2, 0])
            stud("Leg-Front Left x",height,timber_w,timber_t);
        
        rotate([0,0,90])
            translate([timber_w/2-timber_t, -timber_t/2, 0])
                stud("Leg-Front left y",height,timber_w,timber_t);
        
    } else if (location == "+x_+y") {
        translate([-timber_w/2, timber_t/2, 0])
            stud("Leg-Back Left x",height,timber_w,timber_t);
        
        rotate([0,0,90])
            translate([-timber_w/2+timber_t, -timber_t/2, 0])
                stud("Leg-Back Left y",height,timber_w,timber_t);
        
    } else if (location == "-x_+y") {
        translate([timber_w/2, timber_t/2, 0])
            stud("Leg-Back Right x",height,timber_w,timber_t);
        
        rotate([0,0,90])
            translate([-timber_w/2+timber_t, timber_t/2, 0])
                stud("Leg-Back Right y",height,timber_w,timber_t);
    } else if (location == "-x_-y") {
        translate([timber_w/2, -timber_t/2, 0])
            stud("Leg-Front Right x",height,timber_w,timber_t);
        
        rotate([0,0,90])
            translate([timber_w/2-timber_t, timber_t/2, 0])
                stud("Leg-Front Right y",height,timber_w,timber_t);
    }
}
