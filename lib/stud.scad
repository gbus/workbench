use <MCAD/boxes.scad>

$fa=1;
$fs=0.4;

Pine = [0.85, 0.7, 0.45];

// Vertical stud with rounded corners
module v_stud(l,w,t) {
    r = (w+t)*.03;  // make the rounded corners proportional to
                    // 1/30 of width+tickness
    roundedBox(size=[w,t,l],radius=r,sidesonly=true);
}


// Stud
// Create a stud measuring length x width x tickness
// hv values:
//  "v"   -> vertical stud (along axes z)
//  "h_x" -> horizontal (along axes x)
//  "h_y" -> horizontal (along axes y)
module stud(name="",l,side1,side2,hv="v",col=Pine) {
	echo(name, l);
    color(col)
    
	if (hv=="h_y") {
        rotate([90,90,0]) v_stud(l,side1,side2);
	} else if (hv=="h_x") {
        rotate([0,90,0]) v_stud(l,side1,side2);
	} else {
        v_stud(l,side1,side2);
    }
}

