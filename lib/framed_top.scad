use <frame.scad>

$fa=1;
$fs=0.4;

FiberBoard = [0.7, 0.67, 0.6];



// Framed Top
// Create a stud frame with a sheet panel on top
// The panel can overhang the length and width of the frame [l,w]
// Set [0,0] if the top panel needs to be aligned to the frame 
module framed_top(name="", timber_w, timber_t, l, w, t, overhang=[0,0], explode=false, explode_mul=2) {
    
    // Lift the panel if exploded
    lift_val = explode ? timber_t * explode_mul : 0;
    
	echo("Framed top: ", name);
    
    
    panel_l = l + 2 * overhang[0];
    panel_w = w + 2 * overhang[1];
    
    color(FiberBoard)
    translate([0, 0, t/2+lift_val])
        cube([panel_l, panel_w, t], center = true);
    
    translate([0, 0, -timber_w/2])
        frame(timber_w, timber_t, l, w, explode, explode_mul);
}
