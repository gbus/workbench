/*
 * Frame example.
 * 
 * Draw stud frame examples using the library module
 * One frame is drawn fully assembled and another one on top is shown exploded.
 */
 
use <../lib/frame.scad>

timber_w = 95;
timber_t = 45;
frame_length = 1220;
frame_width = 610;

frame(timber_w, timber_t, frame_length, frame_width);

translate([0, 0, 400])
    frame(timber_w, timber_t, frame_length, frame_width, explode=true);