/*
 * Framed top example.
 * 
 * Draw stud framed top using the library module
 * One frame is drawn fully assembled and another one on top is shown exploded.
 */
 
use <../lib/framed_top.scad>

timber_w = 95;
timber_t = 45;
frame_length = 1220;
frame_width = 610;

top_tickness = 18;
overhang_l_w = [50,20];


framed_top("Table_top", timber_w, timber_t, frame_length, frame_width, top_tickness, overhang_l_w, explode=false, explode_mul=2);