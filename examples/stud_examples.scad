use <../lib/stud.scad>

// Draws three stud lengths parallel to the three axes
$fa=1;
$fs=0.4;

translate([100, 100, 0])
    stud("Vertical stud", 1200, 95, 45, "v", "yellow");

translate([100, 800, 0])
    stud("Horizontal (x) stud", 1200, 95, 45, "h_x", "green");

translate([800, 100, 0])
    stud("Horizontal (y)", 1200, 95, 45, "h_y", "red");