/*
 * Leg examples.
 * 
 * Draw the four table legs using the library module
 */
 
use <../lib/leg.scad>

timber_w = 95;
timber_t = 45;
height = 800;
translate([400, -200, 0])
    leg(timber_w, timber_t, height, "+x_-y");


translate([400, 200, 0])
    leg(timber_w, timber_t, height, "+x_+y");


translate([-400, 200, 0])
    leg(timber_w, timber_t, height, "-x_+y");

translate([-400, -200, 0])
    leg(timber_w, timber_t, height, "-x_-y");