# workbench

Draws a model of a workbench in OpenScad


## Overview

This model can be used to build a table workbench with easy to find wood materials.
If you decide to build your own workbench, in order to acieve the best precision, it is important to rely on standard and profesionally cut parts.
For this reason, a 3D model with parametric values is useful to plan and interactively visualize the final product in advance, with a list of parts and their respective size.

## Disclaimer

I can't guarantee that the generated values are correct. If you decide to use this script to build the workbench it is your responsibility to validate all the measurements reported in openscad. This project is a reviewed and updated version of the original script I used to build my own workbench; in my case the final result was surprisingly good and accurate. 

## How-to use it

1. Install openscad for your OS. If you use linux it is available as package for your distribution.
2. Clone/Download the repo
3. Open from openscad one of the examples or the workbench.scad file
4. Change the parameters to see how the model looks with different values


## OpenScad tutorial

This project is a practical guide that can also be used as a tutorial to learn step-by-step how the openscad library was designed:


1. Stud

Draw a piece of wood with size length x width x tickness. 

By default it is drawn vertically, but a further parameter (hv) can be used to draw the stud horizontally along the axes x or y (ie: "h_x" or "h_y") 

![Stud](imgs/01_stud.png)*Simple stud example*


2. Frame

Draw a frame made of timber studs. 

The function expects timber width and tickness and the overall length and width of the frame. A simplified visualization of the assembly is possible setting explode to true.

![Frame](imgs/02_frame.png)*Frame example*


3. Framed top

Draw a frame with a sheet panel on top of it. 

The function expects timber width and tickness and the overall length and width of the frame. The top panel requires the additional values for tickness and overhang values in the two directions of the frame ([length, width]).  A simplified visualization of the assembly is possible setting explode to true.

![Framed top](imgs/03_framed_top.png)*Framed top example*


4. Legs

Draw a workbench leg.

Given timber width/tickness and height of the leg, two pieces of timber stud are generated in an L-shape configuration. A mandatory location is needed to orientate the leg according to the corner that needs to be drawn.
Values for the corner location are:
"+x_-y": Front Left
"+x_+y": Back Left
"-x_+y": Back Right
"-x_-y": Front Right

![Legs](imgs/04_legs.png)*Four corner legs example*


5. Workbench model

Draw the final workbench assembly

This model reflects the final build I created using 4x2 timber studs and two 18mm MDF sheet panels. For a list of parts and relative measurements, refer to the console output.
One final remark: I have built this workbench with new materials of the same size, but each part can be customised to use different sizes; for example the shelf can be made of thinner timber or the legs can be made of bulkier studs.

![Workbench](imgs/05_workbench.png)*Assembled workbench*


The 3d visualization helps to assemble the workbench, and setting "explode" to true it is possible to have an exploded view of the model.

![Exploded workbench](imgs/06_exploded_workbench.png)*Exploded view of the workbench*
